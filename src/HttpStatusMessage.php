<?php
/**
 * MIT License
 *
 * Copyright (c) 2017 Vilmos "Chilly" Kovacs
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PhpWedge\Core;


interface HttpStatusMessage
{
    /********************
     * 1xx status codes *
     ********************/

    /** HTTP Status: Continue */
    const HTTP_CONTINUE = 'Continue';

    /** HTTP Status: Switching Protocols */
    const HTTP_SWITCHING_PROTOCOLS = 'Switching Protocols';

    /** HTTP Status: Processing (WebDAV) */
    const HTTP_PROCESSING = 'Processing';



    /********************
     * 2xx status codes *
     ********************/

    /** HTTP Status: OK */
    const HTTP_OK = 'OK';

    /** HTTP Status: Created */
    const HTTP_CREATED = 'Created';

    /** HTTP Status: Accepted */
    const HTTP_ACCEPTED = 'Accepted';

    /** HTTP Status: Non-Authoritative Information */
    const HTTP_NON_AUTHORITATIVE_INFORMATION = 'Non-Authoritative Information';

    /** HTTP Status: No Content */
    const HTTP_NO_CONTENT = 'No Content';

    /** HTTP Status: Reset Content */
    const HTTP_RESET_CONTENT = 'Reset Content';

    /** HTTP Status: Partial Content */
    const HTTP_PARTIAL_CONTENT = 'Partial Content';

    /** HTTP Status: Multi-Status (WebDAV) */
    const HTTP_MULTI_STATUS = 'Multi-Status';

    /** HTTP Status: Already Reported (WebDAV) */
    const HTTP_ALREADY_REPORTED = 'Already Reported';

    /** HTTP Status: IM Used */
    const HTTP_IM_USED = 'IM Used';



    /********************
     * 3xx status codes *
     ********************/

    /** HTTP Status: Multiple Choices */
    const HTTP_MULTIPLE_CHOICES = 'Multiple Choices';

    /** HTTP Status: Moved Permanently */
    const HTTP_MOVED_PERMANENTLY = 'Moved Permanently';

    /** HTTP Status: Found */
    const HTTP_FOUND = 'Found';

    /** HTTP Status: See Other */
    const HTTP_SEE_OTHER = 'See Other';

    /** HTTP Status: Not Modified */
    const HTTP_NOT_MODIFIED = 'Not Modified';

    /** HTTP Status: Use Proxy */
    const HTTP_USE_PROXY = 'Use Proxy';

    /** HTTP Status: Unused */
    const HTTP_UNUSED = 'Unused';

    /** HTTP Status: Temporary Redirect */
    const HTTP_TEMPORARY_REDIRECT = 'Temporary Redirect';

    /** HTTP Status: Permanent Redirect (experimental) */
    const HTTP_PERMANENT_REDIRECT = 'Permanent Redirect';



    /********************
     * 4xx status codes *
     ********************/

    /** HTTP Status: Bad Request */
    const HTTP_BAD_REQUEST = 'Bad Request';

    /** HTTP Status: Unauthorized */
    const HTTP_UNAUTHORIZED = 'Unauthorized';

    /** HTTP Status: Payment Required */
    const HTTP_PAYMENT_REQUIRED = 'Payment Required';

    /** HTTP Status: Forbidden */
    const HTTP_FORBIDDEN = 'Forbidden';

    /** HTTP Status: Not Found */
    const HTTP_NOT_FOUND = 'Not Found';

    /** HTTP Status: Method Not Allowed */
    const HTTP_METHOD_NOT_ALLOWED = 'Method Not Allowed';

    /** HTTP Status: Not Acceptable */
    const HTTP_NOT_ACCEPTABLE = 'Not Acceptable';

    /** HTTP Status: Proxy Authentication Required */
    const HTTP_PROXY_AUTHENTICATION_REQUIRED = 'Proxy Authentication Required';

    /** HTTP Status: Request Timeout */
    const HTTP_REQUEST_TIMEOUT = 'Request Timeout';

    /** HTTP Status: Conflict */
    const HTTP_CONFLICT = 'Conflict';

    /** HTTP Status: Gone */
    const HTTP_GONE = 'Gone';

    /** HTTP Status: Length Required */
    const HTTP_LENGTH_REQUIRED = 'Length Required';

    /** HTTP Status: Precondition Failed */
    const HTTP_PRECONDITION_FAILED = 'Precondition Failed';

    /** HTTP Status: Request Entity Too Large */
    const HTTP_REQUEST_ENTITY_TOO_LARGE = 'Request Entity Too Large';

    /** HTTP Status: Request-URI Too Long */
    const HTTP_REQUEST_URI_TOO_LONG = 'Request-URI Too Long';

    /** HTTP Status: Unsupported Media Type */
    const HTTP_UNSUPPORTED_MEDIA_TYPE = 'Unsupported Media Type';

    /** HTTP Status: Requested Range Not Satisfiable */
    const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 'Requested Range Not Satisfiable';

    /** HTTP Status: Expectation Failed */
    const HTTP_EXPECTATION_FAILED = 'Expectation Failed';

    /** HTTP Status: I'm a teapot (RFC 2324) */
    const HTTP_IM_A_TEAPOT = 'I\'m a teapot';

    /** HTTP Status: Enhance Your Calm (Twitter) */
    const HTTP_ENHANCE_YOUR_CALM = 'Enhance Your Calm';

    /** HTTP Status: Unprocessable Entity (WebDAV) */
    const HTTP_UNPROCESSABLE_ENTITY = 'Unprocessable Entity';

    /** HTTP Status: Locked (WebDAV) */
    const HTTP_LOCKED = 'Locked';

    /** HTTP Status: Failed Dependency (WebDAV) */
    const HTTP_FAILED_DEPENDENCY = 'Failed Dependency';

    /** HTTP Status: Reserved for WebDAV */
    const HTTP_RESERVED_FOR_WEBDAV = 'Reserved for WebDAV';

    /** HTTP Status: Upgrade Required */
    const HTTP_UPGRADE_REQUIRED = 'Upgrade Required';

    /** HTTP Status: Precondition Required */
    const HTTP_PRECONDITION_REQUIRED = 'Precondition Required';

    /** HTTP Status: Too Many Requests */
    const HTTP_TOO_MANY_REQUESTS = 'Too Many Requests';

    /** HTTP Status: Request Header Fields Too Large */
    const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = 'Request Header Fields Too Large';

    /** HTTP Status: No Response (Nginx) */
    const HTTP_NO_RESPONSE = 'No Response';

    /** HTTP Status: Retry With (Microsoft) */
    const HTTP_RETRY_WITH = 'Retry With';

    /** HTTP Status: Blocked by Windows Parental Controls (Microsoft) */
    const HTTP_BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS = 'Blocked by Windows Parental Controls';

    /** HTTP Status: Unavailable For Legal Reasons */
    const HTTP_UNAVAILABLE_FOR_LEGAL_REASONS = 'Unavailable For Legal Reasons';

    /** HTTP Status: Client Closed Request (Nginx) */
    const HTTP_CLIENT_CLOSED_REQUEST = 'Client Closed Request';



    /********************
     * 5xx status codes *
     ********************/

    /** HTTP Status: Internal Server Error */
    const HTTP_INTERNAL_SERVER_ERROR = 'Internal Server Error';

    /** HTTP Status: Not Implemented */
    const HTTP_NOT_IMPLEMENTED = 'Not Implemented';

    /** HTTP Status: Bad Gateway */
    const HTTP_BAD_GATEWAY = 'Bad Gateway';

    /** HTTP Status: Service Unavailable */
    const HTTP_SERVICE_UNAVAILABLE = 'Service Unavailable';

    /** HTTP Status: Gateway Timeout */
    const HTTP_GATEWAY_TIMEOUT = 'Gateway Timeout';

    /** HTTP Status: HTTP Version Not Supported */
    const HTTP_HTTP_VERSION_NOT_SUPPORTED = 'HTTP Version Not Supported';

    /** HTTP Status: Variant Also Negotiates (Experimental) */
    const HTTP_VARIANT_ALSO_NEGOTIATES = 'Variant Also Negotiates';

    /** HTTP Status: Insufficient Storage (WebDAV) */
    const HTTP_INSUFFICIENT_STORAGE = 'Insufficient Storage';

    /** HTTP Status: Loop Detected (WebDAV) */
    const HTTP_LOOP_DETECTED = 'Loop Detected';

    /** HTTP Status: Bandwidth Limit Exceeded (Apache) */
    const HTTP_BANDWIDTH_LIMIT_EXCEEDED = 'Bandwidth Limit Exceeded';

    /** HTTP Status: Not Extended */
    const HTTP_NOT_EXTENDED = 'Not Extended';

    /** HTTP Status: Network Authentication Required */
    const HTTP_NETWORK_AUTHENTICATION_REQUIRED = 'Network Authentication Required';

    /** HTTP Status: Network read timeout error */
    const HTTP_NETWORK_READ_TIMEOUT_ERROR = 'Network read timeout error';

    /** HTTP Status: Network connect timeout error */
    const HTTP_NETWORK_CONNECT_TIMEOUT_ERROR = 'Network connect timeout error';
}
