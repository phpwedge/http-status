<?php
/**
 * MIT License
 *
 * Copyright (c) 2017 Vilmos "Chilly" Kovacs
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace PhpWedge\Core;


class HttpStatus
{
    /**
     * HTTP Version 1.0
     */
    const HTTP_VERSION_1_0 = 'HTTP/1.0';

    /**
     * HTTP Version 1.1
     */
    const HTTP_VERSION_1_1 = 'HTTP/1.1';

    /**
     * HTTP Version 2.0
     */
    const HTTP_VERSION_2_0 = 'HTTP/2.0';

    /**
     * @var array
     */
    private $statusMessageForCode = [


        /********************
         * 1xx status codes *
         ********************/

        HttpStatusCode::HTTP_CONTINUE                             => HttpStatusMessage::HTTP_CONTINUE,
        HttpStatusCode::HTTP_SWITCHING_PROTOCOLS                  => HttpStatusMessage::HTTP_SWITCHING_PROTOCOLS,
        HttpStatusCode::HTTP_PROCESSING                           => HttpStatusMessage::HTTP_PROCESSING,


        /********************
         * 2xx status codes *
         ********************/

        HttpStatusCode::HTTP_OK                                   => HttpStatusMessage::HTTP_OK,
        HttpStatusCode::HTTP_CREATED                              => HttpStatusMessage::HTTP_CREATED,
        HttpStatusCode::HTTP_ACCEPTED                             => HttpStatusMessage::HTTP_ACCEPTED,
        HttpStatusCode::HTTP_NON_AUTHORITATIVE_INFORMATION        =>
            HttpStatusMessage::HTTP_NON_AUTHORITATIVE_INFORMATION,
        HttpStatusCode::HTTP_NO_CONTENT                           => HttpStatusMessage::HTTP_NO_CONTENT,
        HttpStatusCode::HTTP_RESET_CONTENT                        => HttpStatusMessage::HTTP_RESET_CONTENT,
        HttpStatusCode::HTTP_PARTIAL_CONTENT                      => HttpStatusMessage::HTTP_PARTIAL_CONTENT,
        HttpStatusCode::HTTP_MULTI_STATUS                         => HttpStatusMessage::HTTP_MULTI_STATUS,
        HttpStatusCode::HTTP_ALREADY_REPORTED                     => HttpStatusMessage::HTTP_ALREADY_REPORTED,
        HttpStatusCode::HTTP_IM_USED                              => HttpStatusMessage::HTTP_IM_USED,


        /********************
         * 3xx status codes *
         ********************/

        HttpStatusCode::HTTP_MULTIPLE_CHOICES                     => HttpStatusMessage::HTTP_MULTIPLE_CHOICES,
        HttpStatusCode::HTTP_MOVED_PERMANENTLY                    => HttpStatusMessage::HTTP_MOVED_PERMANENTLY,
        HttpStatusCode::HTTP_FOUND                                => HttpStatusMessage::HTTP_FOUND,
        HttpStatusCode::HTTP_SEE_OTHER                            => HttpStatusMessage::HTTP_SEE_OTHER,
        HttpStatusCode::HTTP_NOT_MODIFIED                         => HttpStatusMessage::HTTP_NOT_MODIFIED,
        HttpStatusCode::HTTP_USE_PROXY                            => HttpStatusMessage::HTTP_USE_PROXY,
        HttpStatusCode::HTTP_UNUSED                               => HttpStatusMessage::HTTP_UNUSED,
        HttpStatusCode::HTTP_TEMPORARY_REDIRECT                   => HttpStatusMessage::HTTP_TEMPORARY_REDIRECT,
        HttpStatusCode::HTTP_PERMANENT_REDIRECT                   => HttpStatusMessage::HTTP_PERMANENT_REDIRECT,


        /********************
         * 4xx status codes *
         ********************/

        HttpStatusCode::HTTP_BAD_REQUEST                          => HttpStatusMessage::HTTP_BAD_REQUEST,
        HttpStatusCode::HTTP_UNAUTHORIZED                         => HttpStatusMessage::HTTP_UNAUTHORIZED,
        HttpStatusCode::HTTP_PAYMENT_REQUIRED                     => HttpStatusMessage::HTTP_PAYMENT_REQUIRED,
        HttpStatusCode::HTTP_FORBIDDEN                            => HttpStatusMessage::HTTP_FORBIDDEN,
        HttpStatusCode::HTTP_NOT_FOUND                            => HttpStatusMessage::HTTP_NOT_FOUND,
        HttpStatusCode::HTTP_METHOD_NOT_ALLOWED                   => HttpStatusMessage::HTTP_METHOD_NOT_ALLOWED,
        HttpStatusCode::HTTP_NOT_ACCEPTABLE                       => HttpStatusMessage::HTTP_NOT_ACCEPTABLE,
        HttpStatusCode::HTTP_PROXY_AUTHENTICATION_REQUIRED        =>
            HttpStatusMessage::HTTP_PROXY_AUTHENTICATION_REQUIRED,
        HttpStatusCode::HTTP_REQUEST_TIMEOUT                      => HttpStatusMessage::HTTP_REQUEST_TIMEOUT,
        HttpStatusCode::HTTP_CONFLICT                             => HttpStatusMessage::HTTP_CONFLICT,
        HttpStatusCode::HTTP_GONE                                 => HttpStatusMessage::HTTP_GONE,
        HttpStatusCode::HTTP_LENGTH_REQUIRED                      => HttpStatusMessage::HTTP_LENGTH_REQUIRED,
        HttpStatusCode::HTTP_PRECONDITION_FAILED                  => HttpStatusMessage::HTTP_PRECONDITION_FAILED,
        HttpStatusCode::HTTP_REQUEST_ENTITY_TOO_LARGE             => HttpStatusMessage::HTTP_REQUEST_ENTITY_TOO_LARGE,
        HttpStatusCode::HTTP_REQUEST_URI_TOO_LONG                 => HttpStatusMessage::HTTP_REQUEST_URI_TOO_LONG,
        HttpStatusCode::HTTP_UNSUPPORTED_MEDIA_TYPE               => HttpStatusMessage::HTTP_UNSUPPORTED_MEDIA_TYPE,
        HttpStatusCode::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE      =>
            HttpStatusMessage::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE,
        HttpStatusCode::HTTP_EXPECTATION_FAILED                   => HttpStatusMessage::HTTP_EXPECTATION_FAILED,
        HttpStatusCode::HTTP_IM_A_TEAPOT                          => HttpStatusMessage::HTTP_IM_A_TEAPOT,
        HttpStatusCode::HTTP_ENHANCE_YOUR_CALM                    => HttpStatusMessage::HTTP_ENHANCE_YOUR_CALM,
        HttpStatusCode::HTTP_UNPROCESSABLE_ENTITY                 => HttpStatusMessage::HTTP_UNPROCESSABLE_ENTITY,
        HttpStatusCode::HTTP_LOCKED                               => HttpStatusMessage::HTTP_LOCKED,
        HttpStatusCode::HTTP_FAILED_DEPENDENCY                    => HttpStatusMessage::HTTP_FAILED_DEPENDENCY,
        HttpStatusCode::HTTP_RESERVED_FOR_WEBDAV                  => HttpStatusMessage::HTTP_RESERVED_FOR_WEBDAV,
        HttpStatusCode::HTTP_UPGRADE_REQUIRED                     => HttpStatusMessage::HTTP_UPGRADE_REQUIRED,
        HttpStatusCode::HTTP_PRECONDITION_REQUIRED                => HttpStatusMessage::HTTP_PRECONDITION_REQUIRED,
        HttpStatusCode::HTTP_TOO_MANY_REQUESTS                    => HttpStatusMessage::HTTP_TOO_MANY_REQUESTS,
        HttpStatusCode::HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE      =>
            HttpStatusMessage::HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE,
        HttpStatusCode::HTTP_NO_RESPONSE                          => HttpStatusMessage::HTTP_NO_RESPONSE,
        HttpStatusCode::HTTP_RETRY_WITH                           => HttpStatusMessage::HTTP_RETRY_WITH,
        HttpStatusCode::HTTP_BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS =>
            HttpStatusMessage::HTTP_BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS,
        HttpStatusCode::HTTP_UNAVAILABLE_FOR_LEGAL_REASONS        =>
            HttpStatusMessage::HTTP_UNAVAILABLE_FOR_LEGAL_REASONS,
        HttpStatusCode::HTTP_CLIENT_CLOSED_REQUEST                => HttpStatusMessage::HTTP_CLIENT_CLOSED_REQUEST,


        /********************
         * 5xx status codes *
         ********************/

        HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR                => HttpStatusMessage::HTTP_INTERNAL_SERVER_ERROR,
        HttpStatusCode::HTTP_NOT_IMPLEMENTED                      => HttpStatusMessage::HTTP_NOT_IMPLEMENTED,
        HttpStatusCode::HTTP_BAD_GATEWAY                          => HttpStatusMessage::HTTP_BAD_GATEWAY,
        HttpStatusCode::HTTP_SERVICE_UNAVAILABLE                  => HttpStatusMessage::HTTP_SERVICE_UNAVAILABLE,
        HttpStatusCode::HTTP_GATEWAY_TIMEOUT                      => HttpStatusMessage::HTTP_GATEWAY_TIMEOUT,
        HttpStatusCode::HTTP_HTTP_VERSION_NOT_SUPPORTED           => HttpStatusMessage::HTTP_HTTP_VERSION_NOT_SUPPORTED,
        HttpStatusCode::HTTP_VARIANT_ALSO_NEGOTIATES              => HttpStatusMessage::HTTP_VARIANT_ALSO_NEGOTIATES,
        HttpStatusCode::HTTP_INSUFFICIENT_STORAGE                 => HttpStatusMessage::HTTP_INSUFFICIENT_STORAGE,
        HttpStatusCode::HTTP_LOOP_DETECTED                        => HttpStatusMessage::HTTP_LOOP_DETECTED,
        HttpStatusCode::HTTP_BANDWIDTH_LIMIT_EXCEEDED             => HttpStatusMessage::HTTP_BANDWIDTH_LIMIT_EXCEEDED,
        HttpStatusCode::HTTP_NOT_EXTENDED                         => HttpStatusMessage::HTTP_NOT_EXTENDED,
        HttpStatusCode::HTTP_NETWORK_AUTHENTICATION_REQUIRED      =>
            HttpStatusMessage::HTTP_NETWORK_AUTHENTICATION_REQUIRED,
        HttpStatusCode::HTTP_NETWORK_READ_TIMEOUT_ERROR           => HttpStatusMessage::HTTP_NETWORK_READ_TIMEOUT_ERROR,
        HttpStatusCode::HTTP_NETWORK_CONNECT_TIMEOUT_ERROR        =>
            HttpStatusMessage::HTTP_NETWORK_CONNECT_TIMEOUT_ERROR,
    ];

    /**
     * Returns the http status message based on the requested status code.
     *
     * @param int $code
     *
     * @return string
     *
     * @throws HttpStatusCodeNotFoundException   When the requested code not found.
     */
    public function getMessage(int $code): string
    {
        if (empty($this->statusMessageForCode[$code])) {
            throw new HttpStatusCodeNotFoundException('HTTP Status code not found! (' . $code . ')');
        }

        return $this->statusMessageForCode[$code];
    }

    /**
     * Returns the HTTP header message based on the requested status code.
     *
     * @param int $code
     * @param string $protocolVersion
     *
     * @return string
     *
     * @throws HttpStatusCodeNotFoundException   When the requested code not found.
     */
    public function getHeader(int $code, string $protocolVersion = self::HTTP_VERSION_1_0): string
    {
        return $protocolVersion . ' ' . $code . ' ' . $this->getMessage($code);
    }
}
