<?php

declare(strict_types=1);

namespace PhpWedge\Core;


interface HttpStatusCode
{
    /********************
     * 1xx status codes *
     ********************/

    /** HTTP Status: Continue */
    const HTTP_CONTINUE = 100;

    /** HTTP Status: Switching Protocols */
    const HTTP_SWITCHING_PROTOCOLS = 101;

    /** HTTP Status: Processing (WebDAV) */
    const HTTP_PROCESSING = 102;



    /********************
     * 2xx status codes *
     ********************/

    /** HTTP Status: OK */
    const HTTP_OK = 200;

    /** HTTP Status: Created */
    const HTTP_CREATED = 201;

    /** HTTP Status: Accepted */
    const HTTP_ACCEPTED = 202;

    /** HTTP Status: Non-Authoritative Information */
    const HTTP_NON_AUTHORITATIVE_INFORMATION = 203;

    /** HTTP Status: No Content */
    const HTTP_NO_CONTENT = 204;

    /** HTTP Status: Reset Content */
    const HTTP_RESET_CONTENT = 205;

    /** HTTP Status: Partial Content */
    const HTTP_PARTIAL_CONTENT = 206;

    /** HTTP Status: Multi-Status (WebDAV) */
    const HTTP_MULTI_STATUS = 207;

    /** HTTP Status: Already Reported (WebDAV) */
    const HTTP_ALREADY_REPORTED = 208;

    /** HTTP Status: IM Used */
    const HTTP_IM_USED = 226;



    /********************
     * 3xx status codes *
     ********************/

    /** HTTP Status: Multiple Choices */
    const HTTP_MULTIPLE_CHOICES = 300;

    /** HTTP Status: Moved Permanently */
    const HTTP_MOVED_PERMANENTLY = 301;

    /** HTTP Status: Found */
    const HTTP_FOUND = 302;

    /** HTTP Status: See Other */
    const HTTP_SEE_OTHER = 303;

    /** HTTP Status: Not Modified */
    const HTTP_NOT_MODIFIED = 304;

    /** HTTP Status: Use Proxy */
    const HTTP_USE_PROXY = 305;

    /** HTTP Status: Unused */
    const HTTP_UNUSED = 306;

    /** HTTP Status: Temporary Redirect */
    const HTTP_TEMPORARY_REDIRECT = 307;

    /** HTTP Status: Permanent Redirect (experimental) */
    const HTTP_PERMANENT_REDIRECT = 308;



    /********************
     * 4xx status codes *
     ********************/

    /** HTTP Status: Bad Request */
    const HTTP_BAD_REQUEST = 400;

    /** HTTP Status: Unauthorized */
    const HTTP_UNAUTHORIZED = 401;

    /** HTTP Status: Payment Required */
    const HTTP_PAYMENT_REQUIRED = 402;

    /** HTTP Status: Forbidden */
    const HTTP_FORBIDDEN = 403;

    /** HTTP Status: Not Found */
    const HTTP_NOT_FOUND = 404;

    /** HTTP Status: Method Not Allowed */
    const HTTP_METHOD_NOT_ALLOWED = 405;

    /** HTTP Status: Not Acceptable */
    const HTTP_NOT_ACCEPTABLE = 406;

    /** HTTP Status: Proxy Authentication Required */
    const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;

    /** HTTP Status: Request Timeout */
    const HTTP_REQUEST_TIMEOUT = 408;

    /** HTTP Status: Conflict */
    const HTTP_CONFLICT = 409;

    /** HTTP Status: Gone */
    const HTTP_GONE = 410;

    /** HTTP Status: Length Required */
    const HTTP_LENGTH_REQUIRED = 411;

    /** HTTP Status: Precondition Failed */
    const HTTP_PRECONDITION_FAILED = 412;

    /** HTTP Status: Request Entity Too Large */
    const HTTP_REQUEST_ENTITY_TOO_LARGE = 413;

    /** HTTP Status: Request-URI Too Long */
    const HTTP_REQUEST_URI_TOO_LONG = 414;

    /** HTTP Status: Unsupported Media Type */
    const HTTP_UNSUPPORTED_MEDIA_TYPE = 415;

    /** HTTP Status: Requested Range Not Satisfiable */
    const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;

    /** HTTP Status: Expectation Failed */
    const HTTP_EXPECTATION_FAILED = 417;

    /** HTTP Status: I'm a teapot (RFC 2324) */
    const HTTP_IM_A_TEAPOT = 418;

    /** HTTP Status: Enhance Your Calm (Twitter) */
    const HTTP_ENHANCE_YOUR_CALM = 420;

    /** HTTP Status: Unprocessable Entity (WebDAV) */
    const HTTP_UNPROCESSABLE_ENTITY = 422;

    /** HTTP Status: Locked (WebDAV) */
    const HTTP_LOCKED = 423;

    /** HTTP Status: Failed Dependency (WebDAV) */
    const HTTP_FAILED_DEPENDENCY = 424;

    /** HTTP Status: Reserved for WebDAV */
    const HTTP_RESERVED_FOR_WEBDAV = 425;

    /** HTTP Status: Upgrade Required */
    const HTTP_UPGRADE_REQUIRED = 426;

    /** HTTP Status: Precondition Required */
    const HTTP_PRECONDITION_REQUIRED = 428;

    /** HTTP Status: Too Many Requests */
    const HTTP_TOO_MANY_REQUESTS = 429;

    /** HTTP Status: Request Header Fields Too Large */
    const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;

    /** HTTP Status: No Response (Nginx) */
    const HTTP_NO_RESPONSE = 444;

    /** HTTP Status: Retry With (Microsoft) */
    const HTTP_RETRY_WITH = 449;

    /** HTTP Status: Blocked by Windows Parental Controls (Microsoft) */
    const HTTP_BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS = 450;

    /** HTTP Status: Unavailable For Legal Reasons */
    const HTTP_UNAVAILABLE_FOR_LEGAL_REASONS = 451;

    /** HTTP Status: Client Closed Request (Nginx) */
    const HTTP_CLIENT_CLOSED_REQUEST = 499;



    /********************
     * 5xx status codes *
     ********************/

    /** HTTP Status: Internal Server Error */
    const HTTP_INTERNAL_SERVER_ERROR = 500;

    /** HTTP Status: Not Implemented */
    const HTTP_NOT_IMPLEMENTED = 501;

    /** HTTP Status: Bad Gateway */
    const HTTP_BAD_GATEWAY = 502;

    /** HTTP Status: Service Unavailable */
    const HTTP_SERVICE_UNAVAILABLE = 503;

    /** HTTP Status: Gateway Timeout */
    const HTTP_GATEWAY_TIMEOUT = 504;

    /** HTTP Status: HTTP Version Not Supported */
    const HTTP_HTTP_VERSION_NOT_SUPPORTED = 505;

    /** HTTP Status: Variant Also Negotiates (Experimental) */
    const HTTP_VARIANT_ALSO_NEGOTIATES = 506;

    /** HTTP Status: Insufficient Storage (WebDAV) */
    const HTTP_INSUFFICIENT_STORAGE = 507;

    /** HTTP Status: Loop Detected (WebDAV) */
    const HTTP_LOOP_DETECTED = 508;

    /** HTTP Status: Bandwidth Limit Exceeded (Apache) */
    const HTTP_BANDWIDTH_LIMIT_EXCEEDED = 509;

    /** HTTP Status: Not Extended */
    const HTTP_NOT_EXTENDED = 510;

    /** HTTP Status: Network Authentication Required */
    const HTTP_NETWORK_AUTHENTICATION_REQUIRED = 511;

    /** HTTP Status: Network read timeout error */
    const HTTP_NETWORK_READ_TIMEOUT_ERROR = 598;

    /** HTTP Status: Network connect timeout error */
    const HTTP_NETWORK_CONNECT_TIMEOUT_ERROR = 599;
}
