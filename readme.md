[![CircleCI](https://circleci.com/bb/phpwedge/http-status/tree/master.svg?style=svg)](https://circleci.com/bb/phpwedge/http-status/tree/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/51ee1015698d485db11963a247af1240)](https://www.codacy.com/app/PhpWedge/http-status?utm_source=phpwedge@bitbucket.org&amp;utm_medium=referral&amp;utm_content=phpwedge/http-status&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/51ee1015698d485db11963a247af1240)](https://www.codacy.com/app/PhpWedge/http-status?utm_source=phpwedge@bitbucket.org&utm_medium=referral&utm_content=phpwedge/http-status&utm_campaign=Badge_Coverage)
[![Latest Stable Version](https://poser.pugx.org/phpwedge/httpstatus/v/stable)](https://packagist.org/packages/phpwedge/httpstatus)
[![Total Downloads](https://poser.pugx.org/phpwedge/httpstatus/downloads)](https://packagist.org/packages/phpwedge/httpstatus)
[![License](https://poser.pugx.org/phpwedge/httpstatus/license)](https://packagist.org/packages/phpwedge/httpstatus)
# PhpWedge Http Status Code and Message Package
>This package contains HTTP status codes and status messages.
### How to setup?
#### via composer cli
````bash
composer require phpwedge/httpstatus
````
#### via composer.json
````json
  "require": {
    "phpwedge/httpstatus": "^1.0.0"
  }
````
### How to use?
````php
<?php
use PhpWedge\Core\HttpStatus;
use PhpWedge\Core\HttpStatusCode;

$httpStatus = new HttpStatus();

// "HTTP/1.0 404 Not Found"
header($httpStatus->getHeader(HttpStatusCode::HTTP_NOT_FOUND));

echo '<h1 align="center">404</h1>';
echo '<h2 align="center">Page not found!</h2>';
````
### Contribution
> If you miss any kind of HTTP status code or functionality, please create an issue or a pull request!
### References
* [http://www.restapitutorial.com/httpstatuscodes.html](http://www.restapitutorial.com/httpstatuscodes.html)

