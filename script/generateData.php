<?php
/**
 * MIT License
 *
 * Copyright (c) 2017 Vilmos "Chilly" Kovacs
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * Removes the brace part.
 *
 * @param $string
 *
 * @return string
 */
function removeBracePart($string): string
{
    return trim(explode('(', $string)[0]);
}

/**
 * Returns the const name.
 *
 * @param $string
 *
 * @return string
 */
function getConstName($string): string
{
    $string = removeBracePart($string);
    $string = strtr(
        $string,
        [
            ' '  => '_',
            '-'  => '_',
            '\'' => '',
            '.'  => '',
        ]
    );

    return strtoupper('HTTP_' . $string);
}

function getStatusCode($filePath)
{
    $f = fopen($filePath, 'r');
    $statusBit = 1;
    echo '/********************' . PHP_EOL;
    echo ' * ' . $statusBit . 'xx status codes *' . PHP_EOL;
    echo ' ********************/' . PHP_EOL;
    echo PHP_EOL;

    while ($row = fgetcsv($f, null, ';')) {
        if ($statusBit < $row[0][0]) {
            $statusBit = $row[0][0];
            echo PHP_EOL . PHP_EOL;
            echo '/********************' . PHP_EOL;
            echo ' * ' . $statusBit . 'xx status codes *' . PHP_EOL;
            echo ' ********************/' . PHP_EOL;
            echo PHP_EOL;
        }
        echo '/** HTTP Status: ' . $row[1] . ' */' . PHP_EOL;
        echo 'const ' . getConstName($row[1]) . ' = ' . $row[0] . ';' . PHP_EOL;
        echo PHP_EOL;
    }
}

function getStatusMessage($filePath)
{
    $f = fopen($filePath, 'r');
    $statusBit = 1;
    echo '/********************' . PHP_EOL;
    echo ' * ' . $statusBit . 'xx status codes *' . PHP_EOL;
    echo ' ********************/' . PHP_EOL;
    echo PHP_EOL;

    while ($row = fgetcsv($f, null, ';')) {
        if ($statusBit < $row[0][0]) {
            $statusBit = $row[0][0];
            echo PHP_EOL . PHP_EOL;
            echo '/********************' . PHP_EOL;
            echo ' * ' . $statusBit . 'xx status codes *' . PHP_EOL;
            echo ' ********************/' . PHP_EOL;
            echo PHP_EOL;
        }
        echo '/** HTTP Status: ' . $row[1] . ' */' . PHP_EOL;
        echo 'const ' . getConstName($row[1]) . ' = \'' . addslashes(removeBracePart($row[1])) . '\';' . PHP_EOL;
        echo PHP_EOL;
    }
}

function getCodeStatusMessage($filePath)
{
    $f = fopen($filePath, 'r');

    $keys = [];
    $maxLength = 0;
    echo '[' . PHP_EOL;
    while ($row = fgetcsv($f, null, ';')) {
        $current = getConstName($row[1]);
        $keys[$row[0][0]][] = $current;
        if (strlen($current) > $maxLength) {
            $maxLength = strlen($current);
        }
    }

    foreach ($keys as $key => $section) {
        echo PHP_EOL . PHP_EOL;
        echo '  /********************' . PHP_EOL;
        echo '   * ' . $key . 'xx status codes *' . PHP_EOL;
        echo '   ********************/' . PHP_EOL;
        echo PHP_EOL;
        foreach ($section as $offset) {
            echo '  HttpStatusCode::' . str_pad($offset, $maxLength, ' ', STR_PAD_RIGHT) .
                ' => HttpStatusMessage::' . $offset . ',' . PHP_EOL;
        }
    }
    echo ']' . PHP_EOL;
}

function getMessageTests($filePath)
{
    $f = fopen($filePath, 'r');

    $keys = [];
    $maxLength = 0;
    echo '[' . PHP_EOL;
    while ($row = fgetcsv($f, null, ';')) {
        $current = getConstName($row[1]);
        $keys[$row[0][0]][] = $current;
        if (strlen($current) > $maxLength) {
            $maxLength = strlen($current);
        }
    }

    foreach ($keys as $key => $section) {
        echo PHP_EOL . PHP_EOL;
        echo '  /********************' . PHP_EOL;
        echo '   * ' . $key . 'xx status codes *' . PHP_EOL;
        echo '   ********************/' . PHP_EOL;
        echo PHP_EOL;
        foreach ($section as $offset) {
            echo '  [HttpStatusCode::' . $offset . ', HttpStatusMessage::' . $offset . '],' . PHP_EOL;
        }
    }
    echo ']' . PHP_EOL;
}

$filePath = __DIR__ . '/../data/http_status.csv';
getStatusCode($filePath);
//getStatusMessage($filePath);
//getCodeStatusMessage($filePath);
//getMessageTests($filePath);
